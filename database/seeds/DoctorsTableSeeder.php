<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Doctor;

class DoctorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Doctor::create(['name' => 'Иванов Иван', 'created_at' => Carbon::now()]);
        Doctor::create(['name' => 'Петров Петр', 'created_at' => Carbon::now()]);
        Doctor::create(['name' => 'Сидоров Илья', 'created_at' => Carbon::now()]);
        Doctor::create(['name' => 'Мухин Андрей', 'created_at' => Carbon::now()]);
        Doctor::create(['name' => 'Краснова Анастасия', 'created_at' => Carbon::now()]);
    }
}
