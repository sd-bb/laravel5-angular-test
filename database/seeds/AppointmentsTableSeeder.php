<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Appointment;

class AppointmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Appointment::create(['datetime' => Carbon::create(2016, 07, 22, 10, 00, 00)->toDateTimeString(), 'doctor_id' => 1, 'created_at' => Carbon::now()]);
        Appointment::create(['datetime' => Carbon::create(2016, 07, 22, 11, 00, 00)->toDateTimeString(), 'doctor_id' => 1, 'created_at' => Carbon::now()]);
        Appointment::create(['datetime' => Carbon::create(2016, 07, 22, 12, 00, 00)->toDateTimeString(), 'doctor_id' => 1, 'created_at' => Carbon::now()]);
        Appointment::create(['datetime' => Carbon::create(2016, 07, 22, 13, 00, 00)->toDateTimeString(), 'doctor_id' => 1, 'created_at' => Carbon::now()]);
        Appointment::create(['datetime' => Carbon::create(2016, 07, 23, 10, 00, 00)->toDateTimeString(), 'doctor_id' => 1, 'created_at' => Carbon::now()]);
        Appointment::create(['datetime' => Carbon::create(2016, 07, 22, 10, 00, 00)->toDateTimeString(), 'doctor_id' => 2, 'created_at' => Carbon::now()]);
        Appointment::create(['datetime' => Carbon::create(2016, 07, 23, 10, 00, 00)->toDateTimeString(), 'doctor_id' => 2, 'created_at' => Carbon::now()]);
        Appointment::create(['datetime' => Carbon::create(2016, 07, 24, 10, 00, 00)->toDateTimeString(), 'doctor_id' => 2, 'created_at' => Carbon::now()]);
        Appointment::create(['datetime' => Carbon::create(2016, 07, 25, 10, 00, 00)->toDateTimeString(), 'doctor_id' => 2, 'created_at' => Carbon::now()]);
        Appointment::create(['datetime' => Carbon::create(2016, 07, 26, 10, 00, 00)->toDateTimeString(), 'doctor_id' => 2, 'created_at' => Carbon::now()]);
        Appointment::create(['datetime' => Carbon::create(2016, 07, 22, 10, 00, 00)->toDateTimeString(), 'doctor_id' => 3, 'created_at' => Carbon::now()]);
        Appointment::create(['datetime' => Carbon::create(2016, 07, 25, 10, 00, 00)->toDateTimeString(), 'doctor_id' => 3, 'created_at' => Carbon::now()]);
        Appointment::create(['datetime' => Carbon::create(2016, 07, 22, 10, 00, 00)->toDateTimeString(), 'doctor_id' => 4, 'created_at' => Carbon::now()]);
        Appointment::create(['datetime' => Carbon::create(2016, 07, 27, 10, 00, 00)->toDateTimeString(), 'doctor_id' => 4, 'created_at' => Carbon::now()]);
        Appointment::create(['datetime' => Carbon::create(2016, 07, 27, 11, 00, 00)->toDateTimeString(), 'doctor_id' => 4, 'created_at' => Carbon::now()]);
        Appointment::create(['datetime' => Carbon::create(2016, 07, 25, 10, 00, 00)->toDateTimeString(), 'doctor_id' => 5, 'created_at' => Carbon::now()]);
    }
}
