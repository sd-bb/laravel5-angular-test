<?php

namespace App\Http\Controllers;

use App\Appointment;
use Illuminate\Http\Request;
use Carbon\Carbon;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $doctor_id = $request->query('doctor_id');
        $date = $request->query('date');
        if (!$doctor_id || !$date) {
            return response()->json([
                'error' => [
                    'exception' => 'You must specify doctor_id and date',
                ]
            ], 404);
        }
        return response()->json(
            Appointment::where('doctor_id', $doctor_id)
            ->where('datetime', '>', new Carbon($date))
            ->where('datetime', '<', (new Carbon($date))->addDay())
            ->orderBy('datetime', 'asc')
            ->get()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $appointment = Appointment::find($id);
        $appointment->update($request->all());

        return response()->json($appointment);
    }
}
