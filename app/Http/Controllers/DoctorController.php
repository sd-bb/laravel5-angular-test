<?php

namespace App\Http\Controllers;

use App\Doctor;
use Redis;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Doctor::get());
    }

    /**
     * Display a appointments listing of the doctor.
     *
     * @param  int  $doctor_id
     * @return \Illuminate\Http\Response
     */
    public function date($doctor_id)
    {
        $dates = Redis::get('doctor:appointment:' . $doctor_id);
        if (!$dates) {
            return response()->json([]);
        }
        return response()->json(json_decode($dates));
    }
}
