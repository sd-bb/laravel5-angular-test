<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{

    protected $fillable  = ['busy'];

    /**
     * Get the doctor.
     */
    public function doctor()
    {
        return $this->belongsTo('App\Doctor');
    }
}
