<?php

namespace App\Providers;

use App\Appointment;
use Illuminate\Support\ServiceProvider;
use App\Doctor;
use Redis;
use Carbon\Carbon;

class DoctorAppointmentsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Doctor::created(function ($doctor) {
            Redis::set('doctor:appointment:' . $doctor->id, json_encode([]));
        });
        Appointment::saved(function ($appointment) {
            $original = $appointment->getOriginal();
            if (!isset($original['busy']) || $appointment->busy != $original['busy']) {
                $data = json_decode(Redis::get('doctor:appointment:' . $appointment->doctor_id));
                $item = null;
                $date = (new Carbon($appointment->datetime))->toDateString();
                foreach ($data as &$struct) {
                    if ($date == $struct->date) {
                        $item = $struct;
                        if (!$appointment->busy) {
                            $struct->count++;
                        }
                        elseif (isset($original['busy']) && !$original['busy']) {
                            $struct->count--;
                        }
                        break;
                    }
                }
                if ($item == null) {
                    array_push($data, ['date' => $date, 'count' => $appointment->busy ? 0 : 1]);
                }
                Redis::set('doctor:appointment:' . $appointment->doctor_id, json_encode($data));
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
