export function RoutesConfig($stateProvider, $urlRouterProvider) {
	'ngInject';

	let getView = (viewName) => {
		return `./views/app/pages/${viewName}/${viewName}.page.html`;
	};

	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('app', {
			abstract: true,
            data: {},//{auth: true} would require JWT auth
			views: {
				header: {
					templateUrl: getView('header')
				},
				footer: {
					templateUrl: getView('footer')
				},
				main: {}
			}
		})
		.state('app.landing', {
            url: '/',
            resolve: {
                doctors: (API) => API.all('doctor').getList()
            },
            views: {
                'main@': {
                    templateUrl: getView('landing'),
                    controller: ($scope, doctors) => {
                        $scope.doctors = doctors;
                    }
                }
            }
        })
        .state('app.landing.doctor', {
            url: 'doctor/{doctor_id}',
            resolve: {
                doctor: (doctors, $stateParams) =>
                    doctors.filter(doctor => doctor.id == $stateParams['doctor_id']).shift(),
                dates: (doctor) => doctor.all('date').getList()
            },
            views: {
                'appointmentDate@app.landing': {
                    template: '<doctor-dates doctor="doctor" dates="dates"></doctor-dates>',
                    controller: ($scope, doctor, dates) => {
                        $scope.doctor = doctor;
                        $scope.dates = dates;
                    }
                }
            }
        })
        .state('app.landing.doctor.date', {
            url: '/date/{date}',
            resolve: {
                times: (doctor, $stateParams, API) =>
                    API
                    .all('appointment')
                    .getList({doctor_id: doctor.id, date: $stateParams['date']})
            },
            views: {
                'appointmentTime@app.landing': {
                    template: '<doctor-times times="times"></doctor-times>',
                    controller: ($scope, times) => {
                        $scope.times = times;
                    }
                }
            }
        })
        .state('app.login', {
			url: '/login',
			views: {
				'main@': {
					templateUrl: getView('login')
				}
			}
		})
        .state('app.register', {
            url: '/register',
            views: {
                'main@': {
                    templateUrl: getView('register')
                }
            }
        })
        .state('app.forgot_password', {
            url: '/forgot-password',
            views: {
                'main@': {
                    templateUrl: getView('forgot-password')
                }
            }
        })
        .state('app.reset_password', {
            url: '/reset-password/:email/:token',
            views: {
                'main@': {
                    templateUrl: getView('reset-password')
                }
            }
        });
}
