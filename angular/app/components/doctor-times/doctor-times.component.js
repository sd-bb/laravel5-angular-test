class DoctorTimesController{
    constructor(ToastService){
        'ngInject';

        this.ToastService = ToastService;
    }

    $onInit(){
    }

    setBusy(time) {
        time.busy = true;
        time.save().then(() => {
            this.ToastService.show('Вы успешно записаны!');
        }, () => {
            time.busy = false;
            this.ToastService.error('Не удалось записаться, попробуйте позже');
        });
    }
}

export const DoctorTimesComponent = {
    templateUrl: './views/app/components/doctor-times/doctor-times.component.html',
    controller: DoctorTimesController,
    controllerAs: 'vm',
    bindings: {
        times: '='
    }
}
