export const DoctorAppointmentComponent = {
    templateUrl: './views/app/components/doctor-appointment/doctor-appointment.component.html',
    controllerAs: 'vm',
    bindings: {
        doctors: '='
    }
}
