class DoctorDatesController{
    constructor($scope, $state, $stateParams){
        'ngInject';

        this.$scope = $scope;
        this.$state = $state;
        this.$stateParams = $stateParams;
    }

    $onInit(){
        this.$scope.date = this.$stateParams.date ? new Date(this.$stateParams.date) : null;
        this.$scope.$watch('date', (date) => {
            if (date) {
                this.$state.go('app.landing.doctor.date', {
                    date: `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
                });
            }
        });
        this.onlyWorkingDatesPredicate = (date) => {
            return this.dates.some((workingDate) => {
                return (new Date(workingDate.date)).toDateString() === date.toDateString() && workingDate.count > 0;
            })
        }
    }

}

export const DoctorDatesComponent = {
    templateUrl: './views/app/components/doctor-dates/doctor-dates.component.html',
    controller: DoctorDatesController,
    controllerAs: 'vm',
    bindings: {
        doctor: '=',
        dates: '='
    }
}
