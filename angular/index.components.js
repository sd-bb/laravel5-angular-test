import {DoctorTimesComponent} from './app/components/doctor-times/doctor-times.component';
import {DoctorDatesComponent} from './app/components/doctor-dates/doctor-dates.component';
import {ResetPasswordComponent} from './app/components/reset-password/reset-password.component';
import {ForgotPasswordComponent} from './app/components/forgot-password/forgot-password.component';
import {LoginFormComponent} from './app/components/login-form/login-form.component';
import {RegisterFormComponent} from './app/components/register-form/register-form.component';
import {DoctorAppointmentComponent} from './app/components/doctor-appointment/doctor-appointment.component';

angular.module('app.components')
	.component('doctorTimes', DoctorTimesComponent)
	.component('doctorDates', DoctorDatesComponent)
	.component('resetPassword', ResetPasswordComponent)
	.component('forgotPassword', ForgotPasswordComponent)
	.component('loginForm', LoginFormComponent)
	.component('registerForm', RegisterFormComponent)
    .component('doctorAppointment', DoctorAppointmentComponent);

